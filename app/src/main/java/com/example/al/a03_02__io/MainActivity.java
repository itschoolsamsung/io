package com.example.al.a03_02__io;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Environment;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//имплементируем OnClickListener для обработки нажатий
public class MainActivity extends Activity implements View.OnClickListener {

	@SuppressLint("SetTextI18n")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//выводим строку в TextView
		((TextView)findViewById(R.id.textViewExternalStorage)).setText(
				//получаем путь к общему внешнему каталогу для хранения файлов
				"External Storage:\n" + Environment.getExternalStorageDirectory().toString()
		);

		//выводим строку в TextView
		((TextView)findViewById(R.id.textViewExternalStoragePublic)).setText(
				//получаем путь к внешнему каталогу для хранения загруженных файлов
				"External Storage Public:\n" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()
		);

		//выводим строку в TextView
		((TextView)findViewById(R.id.textViewAppDir)).setText(
				//получаем путь к каталогу текущего приложения для хранения загруженных файлов
				"App Dir:\n" + getFilesDir().toString()
		);

		//привязываем кнопку buttonWrite к общему обработчику нажатий текущей активности (this)
		findViewById(R.id.buttonWrite).setOnClickListener(this);
		//привязываем кнопку buttonRead к общему обработчику нажатий текущей активности (this)
		findViewById(R.id.buttonRead).setOnClickListener(this);
	}

	//обработчик нажатий
	@Override
	public void onClick(View view) {
		//в обработчик передаётся ссылка на конкретный виджет, на котором произошло нажатие (view)
		switch (view.getId()) {
			//если нажата кнопка buttonWrite
			case R.id.buttonWrite:
				try {
					//создаём экземпляр FileWriter, в конструкторе указываем путь к файлу
					FileWriter fileW = new FileWriter(getFilesDir().toString() + "/test");
					//записываем в файл строку, полученную из editText
					fileW.write( ((EditText)findViewById(R.id.editText)).getText().toString() );
					fileW.flush();
					//показываем тост с сообщением об успехе
					Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT).show();
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			//если нажата кнопка buttonRead
			case R.id.buttonRead:
				try {
					//создаём экземпляр FileReader, в конструкторе указываем путь к файлу
					FileReader fileR = new FileReader(getFilesDir().toString() + "/test");
					int c;
					String s = "";
					//пока метод read() не вернёт -1, прочитываем из файла по одному символу (в формате int)
					while ((c = fileR.read()) != -1) {
						//преобразуем int в char и накапливаем прочитанные символы в строке (плохой, но простой алгоритм)
						s += String.valueOf((char)c);
					}
					//выводим всю полученную строку в textView
					((TextView)findViewById(R.id.textView)).setText(s);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
		}
	}
}
